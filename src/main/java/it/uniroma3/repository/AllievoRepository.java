package it.uniroma3.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Allievo;

public interface AllievoRepository extends CrudRepository<Allievo, Long> {

	public List<Allievo> findByCity(String city);

	public Optional<Allievo> findByMail(String mail);
	
	public List<Allievo> findByNameAndSurname(String name, String surname);

}
