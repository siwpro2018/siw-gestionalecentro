package it.uniroma3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.uniroma3.model.Responsabile;

@Repository("responsabileRepository")
public interface ResponsabileRepository extends JpaRepository<Responsabile, Long> {

	public Responsabile findByUsername(String username);

}