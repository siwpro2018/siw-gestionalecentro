package it.uniroma3.repository;

import java.sql.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;

public interface CorsoRepository extends CrudRepository<Corso, Long> {

	public List<Corso> findByDate(Date date);

	public List<Corso> findByCentro(Centro centro);
	
	public List<Corso> findByName(String name);
	
	public List<Corso> findByDateAndCentro(Date date,Centro centro );

}
