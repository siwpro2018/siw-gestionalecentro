package it.uniroma3.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Corso {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String name;
	
	@Column
	private String durata;
	
	@Column
	private String description;
	
	@Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date date;

	@Column
	private String hour;
	
	@ManyToMany
	private List<Allievo> partecipanti;
	
	@ManyToOne
	private Centro centro;
	
	public Corso() {}

	public Corso(String name, String desc, Date date, String hour, String durata, List<Allievo> partecipanti, Centro centro) {
		this.name = name;
		this.description = desc;
		this.date = date;
		this.hour = hour;
		this.partecipanti = new ArrayList<>();
		this.centro = centro;
		this.durata = durata;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String desc) {
		this.description = desc;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getHour() {
		return hour;
	}
	
	
	public void setHour(String hour) {
		this.hour = hour;
	}

	public List<Allievo> getPartecipanti() {
		return partecipanti;
	}

	public void setPartecipanti(List<Allievo> partecipanti) {
		this.partecipanti = partecipanti;
	}

	public Centro getCentro() {
		return centro;
	}

	public void setCentro(Centro centro) {
		this.centro = centro;
	}

	public String getDurata() {
		return durata;
	}

	public void setDurata(String durata) {
		this.durata = durata;
	}
	
	
}