package it.uniroma3.model;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;

@Entity
public class Allievo {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String name;

	@Column(nullable=false)
	private String surname;

	@Column(nullable=false)
	private String city;
	
	@Column(nullable=false)
	private String mail;
	
	@Column(nullable=false)
	private String phone;

	@Temporal(TemporalType.DATE)
    @DateTimeFormat(pattern="yyyy-MM-dd")
	private Date dateOfBirth;
	
	@Column(nullable=false)
	private String placeOfBirth;
	
	@ManyToMany(mappedBy="partecipanti")
	private List<Corso> corsiSeguiti;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Centro centro;
	
	@Column
	private boolean termini;
	
	public Allievo() {}

	public Allievo(String name, String surname, String city, String mail, String phone, Date dateOfBirth, String placeOfBirth, List<Corso> corsiSeguiti, boolean termini) {
		this.name = name;
		this.surname = surname;
		this.city = city;
		this.mail = mail;
		this.phone = phone;
		this.dateOfBirth = dateOfBirth;
		this.placeOfBirth = placeOfBirth;
		this.corsiSeguiti = new ArrayList<>();
		this.termini =termini;
	}



	public String getName() {
		return this.name;
	}

	public String getSurname() {
		return this.surname;
	}

	public String getCity() {
		return this.city;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public void setCity(String city) {
		this.city = city;
	}


	public String getMail() {
		return mail;
	}


	public void setMail(String mail) {
		this.mail = mail;
	}


	public String getPhone() {
		return phone;
	}


	public void setPhone(String phone) {
		this.phone = phone;
	}

	
	public Date getDateOfBirth() {
		return dateOfBirth;
	}


	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}


	public String getPlaceOfBirth() {
		return placeOfBirth;
	}


	public void setPlaceOfBirth(String placeOfBirth) {
		this.placeOfBirth = placeOfBirth;
	}


	public List<Corso> getCorsiSeguiti() {
		return corsiSeguiti;
	}


	public void setCorsiSeguiti(List<Corso> corsiSeguiti) {
		this.corsiSeguiti = corsiSeguiti;
	}

	public Centro getCentro() {
		return centro;
	}

	public void setCentro(Centro centro) {
		this.centro = centro;
	}

	public boolean isTermini() {
		return termini;
	}

	public void setTermini(boolean termini) {
		this.termini = termini;
	}
	

	
}
