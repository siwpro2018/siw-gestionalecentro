package it.uniroma3.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Centro {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	
	@Column(nullable=false)
	private String name;
	
	@Column(nullable=false)
	private String address;
	
	@Column(nullable=false)
	private String mail;

	@Column(nullable=false)
	private String phone;

	@Column(nullable=false)
	private String city;
	
	@Column(nullable=false)
	private String capienzaMassima;
	
	@OneToMany(mappedBy="centro")
	private List<Corso> corsi;
	
	@OneToMany(mappedBy="centro")
	private List<Responsabile> respensabili;
	
	public Centro() {}

	public Centro(String name, String address, String mail, String phone, String city, String capienzaMassima,
			 List<Allievo> allieviCentro, List<Corso> corsiCentro) {
		this.name = name;
		this.address = address;
		this.mail = mail;
		this.phone = phone;
		this.city = city;
		this.capienzaMassima = capienzaMassima;
		this.corsi = new ArrayList<>();
		this.respensabili= new ArrayList<>();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getCapienzaMassima() {
		return capienzaMassima;
	}

	public void setCapienzaMassima(String capienzaMassima) {
		this.capienzaMassima = capienzaMassima;
	}

	public List<Corso> getCorsiCentro() {
		return corsi;
	}

	public void setCorsiCentro(List<Corso> corsiCentro) {
		this.corsi = corsiCentro;
	}

	public List<Corso> getCorsi() {
		return corsi;
	}

	public void setCorsi(List<Corso> corsi) {
		this.corsi = corsi;
	}

	public List<Responsabile> getRespensabili() {
		return respensabili;
	}

	public void setRespensabili(List<Responsabile> respensabili) {
		this.respensabili = respensabili;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	
}