package it.uniroma3;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Responsabile;
import it.uniroma3.model.Role;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.ResponsabileService;
import it.uniroma3.service.RoleService;

@SpringBootApplication
public class GestionaleCentroSpringApplication {
	
	
	

	@Autowired
	private CorsoService attivitaService;

	@Autowired
	private AllievoService allievoService;
	
	@Autowired
	private CentroService centroService;
	
	@Autowired
	private ResponsabileService responsabileService;
		
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private CorsoService corsoService;

	

	public static void main(String[] args) {
		SpringApplication.run(GestionaleCentroSpringApplication.class, args);
	}

	@PostConstruct
	public void init() {
		
		Role role = new Role(1, "ADMIN");
		roleService.saveRole(role);
		Centro centro = new Centro("Centro studi informatica", "via della Vasca Navale", "mail@mail.it", "3203339901", "Roma", "200", null, null);
		centroService.save(centro);
		Centro centro2 = new Centro("Centro studi economici", "via della Vasca Navale", "mail2@mail.it", "320123131", "Milano", "200", null, null);
		centroService.save(centro2);
		
		
		
		Role role2 = new Role(2, "RESPONSABILE");
		roleService.saveRole(role2);

		Allievo allievo = new Allievo("Luca", "Sacco", "Roma", "mail3@mail.it", "310329013", (new Date()), "Roma", null, true);
		allievo.setCentro(centro);
		allievoService.save(allievo);
		Allievo allievo2 = new Allievo("Pierluigi", "Spedicato", "Lecce", "mail4@mail.it", "20101231", (new Date()), "Lecce", null, true);
		allievo2.setCentro(centro2);
		allievoService.save(allievo2);
		
		
		//Per registrare un nuovo amministratore andare in ../registration

	}
}
