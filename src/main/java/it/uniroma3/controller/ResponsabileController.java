package it.uniroma3.controller;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import it.uniroma3.controller.validator.CentroValidator;
import it.uniroma3.controller.validator.ResponsabileValidator;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.model.Role;
import it.uniroma3.repository.CentroRepository;
import it.uniroma3.repository.ResponsabileRepository;
import it.uniroma3.repository.RoleRepository;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.ResponsabileService;

@Controller
@SessionAttributes("centri")
public class ResponsabileController {

	@Autowired
	private ResponsabileService responsabileService;

	@Autowired
	private ResponsabileValidator validator;

	@Autowired
	private ResponsabileRepository responsabileRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private CentroRepository centroRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	@RequestMapping("/registrazioneResponsabile")
	public String aggiungiResponsabile(Model model) {
		model.addAttribute("responsabile", new Responsabile());
		model.addAttribute("centri", this.centroRepository.findAll());
		return "amministrazione/registraResponsabile";
	}


	@RequestMapping(value = "/responsabile", method = RequestMethod.POST)
	public String newResponsabile(@Valid @ModelAttribute("responsabile") Responsabile responsabile, 
			Model model, BindingResult bindingResult) {
		this.validator.validate(responsabile, bindingResult);
		Responsabile userExists = responsabileService.findResponsabileByUserName(responsabile.getusername());
		if (userExists != null) {
			bindingResult
					.rejectValue("username", "error.user",
							"Username già in uso!");
			
			return "amministrazione/registraResponsabile";

		}
		if (!bindingResult.hasErrors()) {
			
			try {
				Centro centro = centroRepository.findById(responsabile.getCentro().getId()).get();
				responsabile.setCentro(centro);
				
			}
			catch(NullPointerException e) {
				model.addAttribute("erroreEsistenza", true);
				return "amministrazione/registraResponsabile";
			}
			responsabile.setPassword(bCryptPasswordEncoder.encode(responsabile.getPassword()));
			responsabile.setActive(1);
			Role userRole = roleRepository.findByRole("RESPONSABILE");
			responsabile.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
			responsabileRepository.save(responsabile);
			model.addAttribute("centro", responsabile.getCentro());
			return "amministrazione/riepilogoResponsabile";

		}
		return "amministrazione/registraResponsabile";
	}
}
