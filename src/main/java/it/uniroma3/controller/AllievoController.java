package it.uniroma3.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.IllegalFormatException;
import java.util.IllegalFormatWidthException;
import java.util.Map;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import it.uniroma3.controller.validator.AllievoValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.ResponsabileService;

@Controller
public class AllievoController {
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}

	@Autowired
	private ResponsabileService responsabileService;

	@Autowired
	private AllievoService allievoService;

	@Autowired
	private AllievoValidator validator;


	@RequestMapping("/aggiungiAllievo")
	public String aggiungiAllievo(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "allievoForm";
	}

	@RequestMapping(value = "/allievo/{id}", method = RequestMethod.GET)
	public String getAllievo(@PathVariable("id") Long id, Model model) {
		model.addAttribute("allievo", this.allievoService.findById(id));
		try{
			model.addAttribute("centro", this.allievoService.findById(id).getCentro().getName());
			} 
		catch (NullPointerException e) {
			// TODO: handle exception
		}
		return "mostraAllievo";
	}
	

	@RequestMapping("/reportAllievo")
	public String allievi(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "listaAllievi";
	}
	
	@RequestMapping(value = "/datiInseritiAllievo", method = RequestMethod.GET)
	public String reportAllievoId(@Valid @ModelAttribute("allievo")Allievo allievo,Model model) {
			model.addAttribute("allievoOkay","true");
			try{
				model.addAttribute("allievoNuovo", this.allievoService.findById(allievo.getId()));
				
				model.addAttribute("corsiFrequentati",this.allievoService.findById(allievo.getId()).getCorsiSeguiti());
				//model.addAttribute("centro", this.allievoService.findById(id).getCentro().getName());
				} 
			catch (NullPointerException e) {
				// TODO: handle exception
			}
		return "listaAllievi";
	}
	

	@RequestMapping(value = "/allievo/{name}{surname}", method = RequestMethod.GET)
	public String getAllievoByNameAndBySurname(@PathVariable("name") String name, @PathVariable("surname") String surname , Model model) {
		model.addAttribute("allievi", this.allievoService.findByNameAndSurname(name,surname));
		return "listaAllievi";
	}


	@RequestMapping(value = "/allievo", method = RequestMethod.POST)
	public String newAllievo(@Valid @ModelAttribute("allievo") Allievo allievo, 
			Model model, BindingResult bindingResult) {
		this.validator.validate(allievo, bindingResult);

		if (this.allievoService.alreadyExists(allievo)) {
			model.addAttribute("exists", "allievo already exists");
			model.addAttribute("erroreEsistenza","true");
			return "allievoForm";
		}
		else {
			if (!bindingResult.hasErrors()) {
				Authentication auth = SecurityContextHolder.getContext().getAuthentication();
				Responsabile responsabile = responsabileService.findResponsabileByUserName(auth.getName());
				Centro centro = responsabile.getCentro();
				allievo.setCentro(centro);
				this.allievoService.save(allievo);
				SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
				String data = dateFormat.format(allievo.getDateOfBirth());
				Date date = java.sql.Date.valueOf(data);
				model.addAttribute("nascita",date);
				model.addAttribute("allievoInserito",allievo);
				//model.addAttribute("allievi", this.allievoService.findAll());
				return "riepilogoInserimento";
			}
		}
		model.addAttribute("erroreEsistenza","true");
		return "allievoForm";
	}

}
