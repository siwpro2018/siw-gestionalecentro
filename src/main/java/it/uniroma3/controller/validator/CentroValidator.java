package it.uniroma3.controller.validator;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;

@Component
public class CentroValidator implements Validator {

	   @Override
	    public void validate(Object o, Errors errors) {
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "address", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mail", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "capienzaMassima", "required");	
	        Centro centro = (Centro)o;
	        
	        try {
	        	int capienzaMassima = Integer.parseInt(centro.getCapienzaMassima());
			} catch (NumberFormatException e) {
				errors.rejectValue("capienzaMassima", "formato");
			}
	        
	    }

	    @Override
	    public boolean supports(Class<?> aClass) {
	        return Centro.class.equals(aClass);
	    }	
}
