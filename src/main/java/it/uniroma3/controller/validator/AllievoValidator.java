package it.uniroma3.controller.validator;

import java.util.Date;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import it.uniroma3.model.Allievo;

@Component
public class AllievoValidator implements Validator {

	   @Override
	    public void validate(Object o, Errors errors) {
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "surname", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "city", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "mail", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "phone", "required");
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "dateOfBirth", "required");	   
	        ValidationUtils.rejectIfEmptyOrWhitespace(errors, "placeOfBirth", "required");
	       
	        Allievo allievo = (Allievo)o;
	        System.out.println("Vediamo un po" + allievo.isTermini());
	        if(!allievo.isTermini()) {
	        	errors.rejectValue("termini", "required");
	        }
	        
	        try {
	        	
			} catch (NumberFormatException e) {
				errors.rejectValue("phone", "formato");
			}
	        
	        try {
	        	int cellulare = Integer.parseInt(allievo.getPhone());
			} catch (NumberFormatException e) {
				errors.rejectValue("phone", "formato");
			}
	        
	   }

	    @Override
	    public boolean supports(Class<?> aClass) {
	        return Allievo.class.equals(aClass);
	    }	
}
