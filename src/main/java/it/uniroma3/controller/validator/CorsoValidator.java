package it.uniroma3.controller.validator;

import java.sql.Date;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.fasterxml.jackson.databind.exc.InvalidFormatException;

import it.uniroma3.model.Corso;



@Component
public class CorsoValidator implements Validator {


	@Autowired
	private TimeValidator timeValidator;
	@Override
	public void validate(Object o, Errors errors) {
		System.out.println("Entrato qua");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "date", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "durata", "required");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "hour", "required");

		//ValidationUtils.rejectIfEmptyOrWhitespace(errors, "description", "required");
		Corso corso = (Corso)o;
		
		if(!timeValidator.validate(corso.getHour())) {
			errors.rejectValue("hour", "formato");

		}
		
		try {
			int durata = Integer.parseInt(corso.getDurata());
		} catch (NumberFormatException e) {
			errors.rejectValue("durata", "formato");
		}


	}

	@Override
	public boolean supports(Class<?> aClass) {
		return Corso.class.equals(aClass);
	}	
}
