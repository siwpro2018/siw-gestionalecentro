package it.uniroma3.controller;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import it.uniroma3.model.Responsabile;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.model.*;
import it.uniroma3.service.ResponsabileService;

@Controller
public class HomeController {

	@Autowired
	private ResponsabileService responsabileService;
	
	@Autowired
	private AllievoService allievoService;

	@Autowired
	private CorsoService corsoService;
	
	@Autowired
	private CentroService centroService;
	
	@RequestMapping(value="/home", method = RequestMethod.GET)
	public ModelAndView home(){
		ModelAndView modelAndView = new ModelAndView();
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Responsabile user = responsabileService.findResponsabileByUserName(auth.getName());
		int allievi = allievoService.findAll().size();
		int corsi = corsoService.findAll().size();
		int centri = centroService.findAll().size();
		java.util.Date d = new Date();
		
		java.sql.Date dataSql = new java.sql.Date(d.getTime());
		List<Corso> cOggi= corsoService.findByCentroAndDate(dataSql, user.getCentro());
		modelAndView.addObject("userName", user.getusername());
		modelAndView.addObject("allievi", allievi);
		modelAndView.addObject("centri", centri);
		modelAndView.addObject("corsi", corsi);
		modelAndView.addObject("cOggi", cOggi);
		modelAndView.setViewName("home");
		return modelAndView;
	}
	
}
