package it.uniroma3.controller;

import java.sql.Date;
import java.text.SimpleDateFormat;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.boot.web.servlet.server.Session;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.method.annotation.SessionAttributesHandler;

import it.uniroma3.controller.validator.AllievoValidator;
import it.uniroma3.controller.validator.CorsoValidator;
import it.uniroma3.controller.validator.TimeValidator;
import it.uniroma3.model.Allievo;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Responsabile;
import it.uniroma3.service.AllievoService;
import it.uniroma3.service.CentroService;
import it.uniroma3.service.CorsoService;
import it.uniroma3.service.ResponsabileService;

@Controller
@SessionAttributes("allievoCercato")
public class CorsoController {
	@InitBinder
	public void initBinder(WebDataBinder binder) {
	    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	    dateFormat.setLenient(false);
	    binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, false));
	}
	
	@Autowired
	private AllievoService allievoService;

	@Autowired
	private CentroService centroService;

	@Autowired
	private ResponsabileService responsabileService;

	@Autowired
	private CorsoService corsoService;

	@Autowired
	private CorsoValidator validator;

	@RequestMapping("/corsi")
	public String corsi(Model model) {
		model.addAttribute("corsi", this.corsoService.findAll());
		return "listaCorsi";
	}

	@RequestMapping("/aggiungiCorso")
	public String aggiungiCorso(Model model) {
		model.addAttribute("corso", new Corso());
		return "attivitaForm";
	}

	@RequestMapping(value = "/corso/{id}", method = RequestMethod.GET)
	public String getCorso(@PathVariable("id") Long id, Model model) {
		model.addAttribute("corso", this.corsoService.findById(id));
		return "mostraCorso";
	}


	@RequestMapping(value = "/corso/{name}", method = RequestMethod.GET)
	public String getCorsoByName(@PathVariable("name") String name, Model model) {
		model.addAttribute("corsi", this.corsoService.findByName(name));
		return "listaCorsi";
	}
	
	@RequestMapping(value = "/CercaCorsoDate", method = RequestMethod.GET)
	public String getCorsoByDate(@Valid @ModelAttribute("corso") Corso corso, Model model) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Responsabile responsabile = responsabileService.findResponsabileByUserName(auth.getName());
		Centro centro = responsabile.getCentro();
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String data = dateFormat.format(corso.getDate());
		Date dataSql = java.sql.Date.valueOf(data);
		model.addAttribute("corsiOkay","true");
		
		model.addAttribute("corsi", this.corsoService.findByCentroAndDate(dataSql,centro));
		return "cercaCorso";
	}
	
	@RequestMapping(value = "/CercaCorso", method = RequestMethod.GET)
	public String getCorso( Model model) {
		model.addAttribute("corso", new Corso());
		return "cercaCorso";
	}


	@RequestMapping(value = "/corso", method = RequestMethod.POST)
	public String newCorso(@Valid @ModelAttribute("corso") Corso corso, 
			Model model, BindingResult bindingResult) {
		this.validator.validate(corso, bindingResult);

        if (!bindingResult.hasErrors()) {
			//recupera il centro dall'utente corrente
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			Responsabile responsabile = responsabileService.findResponsabileByUserName(auth.getName());
			Centro centro = responsabile.getCentro();
			corso.setCentro(centro);
			centro.getCorsi().add(corso);
			this.centroService.save(centro);
			this.corsoService.save(corso);
			
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String data = dateFormat.format(corso.getDate());
			Date date = java.sql.Date.valueOf(data);
			model.addAttribute("DataInit",date);
			model.addAttribute("corsoInserito",corso);
			return "home";
        }
        else {
            model.addAttribute("exist","true");
            return "attivitaForm";
        }
	}


	
	@RequestMapping("/prenotazioneCorso")
	public String scegliAllievo(Model model) {
		model.addAttribute("allievo", new Allievo());
		return "prenotazione";
	}
	


	@RequestMapping(value = "/cercaAllievoPrenotazione", method = RequestMethod.POST)
	public String cercaAllievo(@ModelAttribute Allievo allievo, Model model,BindingResult bindingResult) {
		AllievoValidator validatorAllievo= new AllievoValidator();
		validatorAllievo.validate(allievo, bindingResult);
		Allievo allievoCercato = this.allievoService.findByMail(allievo.getMail());
		
		
		if (allievoCercato!=null) { 
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    		Responsabile responsabile = responsabileService.findResponsabileByUserName(auth.getName());
    		Centro centro = responsabile.getCentro();
			model.addAttribute("allievoCercato", allievoCercato);
			model.addAttribute("corsi", this.corsoService.findByCentro(centro));
			return "selezionaCorso"; 
		}
		
		else {
			model.addAttribute("notExists", "true");
			return "prenotazione";
		}
	}

	@RequestMapping(value = "/prenotazioneCorso/{id}", method = RequestMethod.GET)
	public String getAttivita(@PathVariable Long id, Model model, @ModelAttribute("allievoCercato") Allievo allievo) {
		Allievo a = this.allievoService.findByMail(allievo.getMail());

		Corso corso= this.corsoService.findById(id);
		model.addAttribute("corso", corso);
		this.allievoService.update(a,corso);
		this.corsoService.update(corso,a);
		model.addAttribute("allievo", a);
		return "riepilogoPrenotazione";
	}
}

