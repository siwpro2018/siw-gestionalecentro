package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Centro;
import it.uniroma3.model.Responsabile;
import it.uniroma3.model.Role;
import it.uniroma3.repository.CentroRepository;
import it.uniroma3.repository.ResponsabileRepository;
import it.uniroma3.repository.RoleRepository;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Service("responsabileService")
public class ResponsabileService {

	@Autowired
	private ResponsabileRepository responsabileRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private CentroRepository centroRepository;
	@Autowired
	private BCryptPasswordEncoder bCryptPasswordEncoder;

	public Responsabile findResponsabileByUserName(String username) {
		return responsabileRepository.findByUsername(username);
	}

	public void saveUser(Responsabile responsabile) {
		responsabile.setPassword(bCryptPasswordEncoder.encode(responsabile.getPassword()));
		responsabile.setActive(1);
		Centro centro = centroRepository.findByName("Centro studi informatica").get();
		responsabile.setCentro(centro);
		Role userRole = roleRepository.findByRole("ADMIN");
		responsabile.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
		responsabileRepository.save(responsabile);
	}

}