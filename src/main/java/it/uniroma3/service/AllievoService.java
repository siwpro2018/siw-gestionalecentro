package it.uniroma3.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Corso;
import it.uniroma3.repository.AllievoRepository;

@Transactional
@Service
public class AllievoService {

	@Autowired
	private AllievoRepository allievoRepository; 

	public Allievo save(Allievo allievo) {
		return this.allievoRepository.save(allievo);
	}

	public List<Allievo> findByCity(String city) {
		return this.allievoRepository.findByCity(city);
	}

	public List<Allievo> findAll() {
		return (List<Allievo>) this.allievoRepository.findAll();
	}

	public Allievo findById(Long id) {
		Optional<Allievo> allievo = this.allievoRepository.findById(id);
		if (allievo.isPresent()) 
			return allievo.get();
		else
			return null;
	}

	public List<Allievo> findByNameAndSurname(String name, String surname) {
		List<Allievo> allievi = (List<Allievo>) this.allievoRepository.findByNameAndSurname(name,surname);
		if (allievi.size()>0) 
			return allievi;
		else
			return null;
	}

	public Allievo findByMail(String mail) {
		Optional<Allievo> allievo = this.allievoRepository.findByMail(mail);
		if (allievo.isPresent()) 
			return allievo.get();
		else
			return null;
	}

	public boolean alreadyExists(Allievo allievo) {
		List<Allievo> allievi = this.allievoRepository.findByNameAndSurname(allievo.getName(), allievo.getSurname());
		if (allievi.size() > 0)
			return true;
		else 
			return false;
	}

	public void update(Allievo allievo,Corso corso) {
		List<Corso> corsi= allievo.getCorsiSeguiti();
		if(corsi!=null) {
			corsi.add(corso);
			allievo.setCorsiSeguiti(corsi);
			
		}
		else {
			corsi= new ArrayList<>();
			corsi.add(corso);
			allievo.setCorsiSeguiti(corsi);
		}
		allievoRepository.save(allievo);
	}	
}
