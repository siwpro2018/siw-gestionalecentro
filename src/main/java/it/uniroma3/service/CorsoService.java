package it.uniroma3.service;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Allievo;
import it.uniroma3.model.Centro;
import it.uniroma3.model.Corso;
import it.uniroma3.model.Responsabile;
import it.uniroma3.repository.AllievoRepository;
import it.uniroma3.repository.CorsoRepository;

@Transactional
@Service
public class CorsoService {

	@Autowired
	private CorsoRepository corsoRepository; 

	@Autowired
	private ResponsabileService responsabileService;

	public Corso save(Corso corso) {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		Responsabile user = responsabileService.findResponsabileByUserName(auth.getName());
		/**corso.setDate(new java.util.Date());
		corso.setHour(new java.util.Date());**/

		return this.corsoRepository.save(corso);
	}
	
	public List<Corso> findByCentroAndDate(Date date, Centro centro){
		return this.corsoRepository.findByDateAndCentro(date, centro);
	}
	
	public List<Corso> findByDate(Date date) {
		return this.corsoRepository.findByDate(date);
	}

	public List<Corso> findAll() {
		return (List<Corso>) this.corsoRepository.findAll();
	}

	public Corso findById(Long id) {
		Optional<Corso> corso= this.corsoRepository.findById(id);
		if (corso.isPresent()) 
			return corso.get();
		else
			return null;
	}

	public List<Corso> findByName(String name) {
		List<Corso> corsi = (List<Corso>) this.corsoRepository.findByName(name);
		if (corsi.size()>0) 
			return corsi;
		else
			return null;
	}

	public List<Corso> findByCentro(Centro centro) {
		List<Corso> corsi = (List<Corso>) this.corsoRepository.findByCentro(centro);
		if (corsi.size()>0) 
			return corsi;
		else
			return null;
	}

	public boolean alreadyExists(Corso corso) {
		List<Corso> corsi= this.corsoRepository.findByName(corso.getName());
		if (corsi.size() > 0)
			return true;
		else 
			return false;
	}
	
	public void update(Corso corso, Allievo allievo) {
		List<Allievo> partecipanti = corso.getPartecipanti();
		if(partecipanti!=null) {
			partecipanti.add(allievo);
			corso.setPartecipanti(partecipanti);
		}
		else {
			partecipanti = new ArrayList<>();
			partecipanti.add(allievo);
			corso.setPartecipanti(partecipanti);
		}
		corsoRepository.save(corso);
	}	
}
