package it.uniroma3.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.uniroma3.model.Responsabile;
import it.uniroma3.model.Role;
import it.uniroma3.repository.ResponsabileRepository;
import it.uniroma3.repository.RoleRepository;

import java.util.Arrays;
import java.util.HashSet;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Service("roleService")
public class RoleService {
	@Autowired
	private RoleRepository roleRepository;
	
	public void saveRole(Role role) {
		roleRepository.save(role);
	}

}